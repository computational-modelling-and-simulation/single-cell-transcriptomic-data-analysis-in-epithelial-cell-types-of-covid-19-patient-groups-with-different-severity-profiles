###########################
# Differential gene expression analisis on scRNAseq data in epithelial lung tissue in COVID-19 and healthy subjects. 
###########################
# R 4.0.3
# Seurat v3.2
# COVID19 vs. CTRL scRNAseq pipeline
# rds dataset from GSE145926 (10.1038/s41591-020-0901-9), GSE160664 (10.1164/rccm.202008-3198OC) 	


#####################################################
# Merging rds databases from 3 BALF Mild COVID-19, 6 BALF Severe COVID-19, 3 BALF Helthy subjects (GSE145926) and 9 Epithelieal Biopses Healthy (GSE160664).
#####################################################
library(Seurat)
library(hdf5r)
library(dplyr)
library(patchwork)

setwd("./Input/Raw_data_rds")


# GSE145926 (10.1038/s41591-020-0901-9)
# Data already filtered as reported in Liao et al. 2020
Liao_immune <- readRDS("GSE145926_Liao_immune_combined.rds")


# GSE160664 (10.1164/rccm.202008-3198OC) 
# Data already filtered as reported in Okuda et al. 2021

meta.epit<-read.table("GSE160664_Cedars_all_metaData.txt")
immunet_epit <- readRDS("GSE160664_Cedars_all_seurat.rds")
immunet_epit_meta<-AddMetaData(immunet_epit, meta.epit)

# Epithelial celltype classification reported in Okuda et al., 2021 was used to confirm. 

immunet_epit_meta@meta.data$celltype <- immunet_epit_meta@meta.data$OrigCellType
immunet_epit_meta@meta.data$stim <- "CTRL"




ifnb.list <- list(Liao_immune, immunet_epit_meta)

immunet_epit.anchors <- FindIntegrationAnchors(object.list = ifnb.list, dims = 1:50)
immunet_epit.combined <- IntegrateData(anchorset = immunet_epit.anchors, dims = 1:50)
DefaultAssay(immunet_epit.combined) <- "integrated"

# Run the standard workflow for visualization and clustering
immunet_epit.combined <- ScaleData(immunet_epit.combined, verbose = FALSE)
immunet_epit.combined <- RunPCA(immunet_epit.combined, npcs = 50, verbose = FALSE)
# t-SNE and Clustering
immunet_epit.combined <- RunUMAP(immunet_epit.combined, reduction = "pca", dims = 1:50)
immunet_epit.combined <- FindNeighbors(immunet_epit.combined, reduction = "pca", dims = 1:50)
immunet_epit.combined <- FindClusters(immunet_epit.combined, resolution = 0.6)
# res. 0.6 = 35 clusters (0-34)

# Best representation celltypes
immunet_epit.combined <- FindClusters(immunet_epit.combined, resolution = 0.9)
# res. 0.7 = 45 clusters (0-44)

Idents(immunet_epit.combined) <- "stim"

DimPlot(immunet_epit.combined, reduction = "umap", label= T, repel=T, split.by="stim")

umap<-DimPlot(immunet_epit.combined, reduction = "umap", label= T, repel=T)

pdf(file='niarakis_umap.pdf', width=10, height=10)
print(umap)
dev.off()

setwd("../.")

saveRDS(immunet_epit.combined,  "niarakis_combined_nobio_old_res_3.rds")





#####################################
# Celltypes identification in clusters by multiple cell markers
#####################################

DefaultAssay(immunet_epit.combined) <- "RNA"
# generic markers
#marker come riportato in Liao et al., 2020 
#macrophages (CD68), 
#neutrophils (FCGR3B), 
#myeloid dendritic cells (mDCs) (CD1C, CLEC9A), 
#plasmacytoid dendritic cells (pDCs) (LILRA4), 
#natural killer (NK) cells (KLRD1), 
#T cells (CD3D), B cells (MS4A1), 
#plasma cells (IGHG4) 
#and epithelial cells (TPPP3, KRT18)
DefaultAssay(immunet_epit.combined) <- "RNA"
#epithelial cells 
FeaturePlot(immunet_epit.combined, features = c("TPPP3", "KRT18"), min.cutoff= 0, max.cutoff= 10, label = T, repel = T)

FeaturePlot(immunet_epit.combined, features = c("CD68"), min.cutoff= 0, max.cutoff= 10, label = T, repel = T)
FeaturePlot(immunet_epit.combined, features = c("FCGR3B"), min.cutoff= 0, max.cutoff= 10, label = T, repel = T)
FeaturePlot(immunet_epit.combined, features = c("CD1C", "CLEC9A"), min.cutoff= 0, max.cutoff= 10, label = T, repel = T)
FeaturePlot(immunet_epit.combined, features = c("LILRA4"), min.cutoff= 0, max.cutoff= 10, label = T, repel = T)
FeaturePlot(immunet_epit.combined, features = c("KLRD1"), min.cutoff= 0, max.cutoff= 10, label = T, repel = T)
FeaturePlot(immunet_epit.combined, features = c("CD3D"), min.cutoff= 0, max.cutoff= 10, label = T, repel = T)
FeaturePlot(immunet_epit.combined, features = c("MS4A1"), min.cutoff= 0, max.cutoff= 10, label = T, repel = T)
FeaturePlot(immunet_epit.combined, features = c("IGHG4"), min.cutoff= 0, max.cutoff= 10, label = T, repel = T)




FeaturePlot(immunet_epit.combined, features = c("CD68", "FCGR3B", "CD1C", "CLEC9A", "LILRA4", "KLRD1", "CD3D", "MS4A1", "IGHG4", "TPPP3", "KRT18"), min.cutoff= 0, max.cutoff= 10, label = T)

# Okuda et al., 2019 (DOI: 10.1164/rccm.201911-2199OC) markers health epitelial Cells

#secretory
FeaturePlot(immunet_epit.combined, features = c("SCGB1A1", "SCGB3A1"), min.cutoff= 0, max.cutoff= 10, label = T, repel = T)
FeaturePlot(immunet_epit.combined, features = c("MUC5B", "MUC5AC"), min.cutoff= 0, max.cutoff= 10, label = T, repel = T)

#basal
FeaturePlot(immunet_epit.combined, features = c("KRT5", "KRT15"), min.cutoff= 0, max.cutoff= 10, label = T, repel = T)

#ciliated
FeaturePlot(immunet_epit.combined, features = c("DNAH5", "RSPH1", "TMEM190"), min.cutoff= 0, max.cutoff= 10, label = T, repel = T)




# Deprez et al., 2019 (DOI: 10.1164/rccm.201911-2199OC) markers health epitelial Cells

DefaultAssay(immunet_epit.combined) <- "RNA"
# Basal cells
FeaturePlot(immunet_epit.combined, features = c("KRT5", "TP63", "DLK2"), min.cutoff= 0, max.cutoff= 10, label = T, repel = T)


# Suprabasal cells
FeaturePlot(immunet_epit.combined, features = c("KRT5", "SERPINB4", "KRT19"), min.cutoff= 0, max.cutoff= 10, label = T, repel = T)


# Secretory
FeaturePlot(immunet_epit.combined, features = c("SCGB1A1", "TSPAN8", "MUC5AC"), min.cutoff= 0, max.cutoff= 10, label = T, repel = T)

# Multiciliated
FeaturePlot(immunet_epit.combined, features = c("FOXJ1", "PIFO", "RYR3"), min.cutoff= 0, max.cutoff= 10, label = T, repel = T)

# AT1
FeaturePlot(immunet_epit.combined, features = c("HOPX", "AGER", "SPOCK2"), min.cutoff= 0, max.cutoff= 10, label = T, repel = T)
# AT2
FeaturePlot(immunet_epit.combined, features = c("SFTPB", "SFTPC", "SFTPD"), min.cutoff= 0, max.cutoff= 10, label = T, repel = T)



###################################################################################
#human proteinatlas
#https://www.proteinatlas.org/humanproteome/celltype/epithelial+cells
#Ciliated cells
FeaturePlot(immunet_epit.combined, features = c("CFAP157", "FAM92B"), min.cutoff= 0, max.cutoff= 10, label = T, repel = T)

#Club cells
FeaturePlot(immunet_epit.combined, features = c("SCGB1A1", "SLPI"), min.cutoff= 0, max.cutoff= 10, label = T, repel = T)

#Alveolar cells type 1
FeaturePlot(immunet_epit.combined, features = c("AQP4", "CLDN18"), min.cutoff= 0, max.cutoff= 10, label = T, repel = T)

#Alveolar cells type 2
FeaturePlot(immunet_epit.combined, features = c("NAPSA", "SFTPC"), min.cutoff= 0, max.cutoff= 10, label = T, repel = T)



###################################################################################
#https://www.proteinatlas.org/ENSG00000117298-ECE1/celltype/lung
# marker atlas

#Alveolar cells type 1
FeaturePlot(immunet_epit.combined, features = c("CAV1", "AGER", "EMP2"), min.cutoff= 0, max.cutoff= 10, label = T, repel = T)  


#Alveolar cells type 2
FeaturePlot(immunet_epit.combined, features = c("LAMP3", "NAPSA", "SFTPA2"), min.cutoff= 0, max.cutoff= 10, label = T, repel = T) 

#Ciliated cells
FeaturePlot(immunet_epit.combined, features = c("CFAP53", "RSPH4A", "SNTN"), min.cutoff= 0, max.cutoff= 10, label = T, repel = T)


#Club cells
FeaturePlot(immunet_epit.combined, features = c("BPIFB1", "SCGB1A1", "SCGB3A1"), min.cutoff= 0, max.cutoff= 10, label = T, repel = T)

# Completely or partially empties columns were deleted in metadata.




###################################################################################
# assign celltypes per each cluster

Idents(immunet_epit.combined) <- "seurat_clusters"
immunet_epit.combined@meta.data$celltype_res_name <- immunet_epit.combined@meta.data$seurat_clusters
Idents(immunet_epit.combined) <- "celltype_res_name"
DefaultAssay(immunet_epit.combined) <- "integrated"

DimPlot(immunet_epit.combined, reduction = "umap",label= T)

data = immunet_epit.combined

Idents(immunet_epit.combined) <- "seurat_clusters"
immunet_epit.combined@meta.data$celltype_res_name <- data@meta.data$seurat_clusters
Idents(immunet_epit.combined) <- "celltype_res_name"
DefaultAssay(immunet_epit.combined)<-"integrated"
immunet_epit.combined <- RenameIdents(immunet_epit.combined, '0' = "Macrophages", '1' = "Macrophages", '2' = "Macrophages", '3' = "Macrophages", '4' = "Alveolar cells type 2",'5' = "Macrophages", '6' = "Macrophages", '7' = "Natural Killer", '8' = "Macrophages", '9' = "Alveolar cells type 2", '10' = "Ciliated cells", '11' = "T cells", '12' = "Ciliated cells", '13' = "Basal cells", '14' = "Alveolar cells type 2", '15' = "Suprabasal cells", '16' = "T cells", '17' = "Secretory cells", '18' = "Alveolar cells type 2")
immunet_epit.combined <- RenameIdents(immunet_epit.combined, '19' = "Alveolar cells type 1", '20' = "Alveolar cells type 2",'21' = "Secretory cells", '22' = "Basal cells",'23' = "Suprabasal cells", '24' = "Plasma cells")
immunet_epit.combined <- RenameIdents(immunet_epit.combined, '25' = "Macrophages", '26' = "Macrophages", '27' = "Natural Killer", '28' = "Dendritic cells", '29' = "Macrophages", '30' = "Neutrophils", '31' = "Basal cells", '32' = "Alveolar cells type 1", '33' = "Secretory cells", '34' = "Erythroid cells", '35' = "Ciliated cells", '36' = "Ciliated cells", '37' = "Alveolar cells type 2", '38' = "B cells", '39' = "Ciliated cells", '40' = "Ciliated cells", '41' = "Plasma cells", '42' = "Plasma cells", '43' = "Plasma cells")

DimPlot(data, reduction = "umap",label= T)

immunet_epit.combined@meta.data$celltype <- as.factor(Idents(immunet_epit.combined))
immunet_epit.combined@meta.data$celltype_name <- NULL
Idents(immunet_epit.combined) <- "celltype"

#find gene markers for cluster
cluster <- FindConservedMarkers(immunet_epit.combined, ident.1 = "1", grouping.var = "stim", only.pos = TRUE, min.diff.pct = 0.25, min.pct = 0.25, logfc.threshold = 0.25)

