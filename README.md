**Single-cell transcriptomic data analysis in epithelial cell types of COVID-19 patient groups with different severity profiles.**


Francesco Messina1

1 National Institute for Infectious Diseases "Lazzaro Spallanzani" IRCCS, Via Portuense, 292 00149 Rome, Italy Rome, Italy. 

Corresponding authors: francesco.messina@inmi.it



**Scope:** Computational model of interaction on COVID-19 have tried to describe the direct connection between SARS-CoV-2 proteins and human host, disregarding specific cells or tissue response induced by SARS-CoV-2 infection or damaging inflammatory response. Here, we provided gene expression analysis to exploring differential expressed genes (DEG) in specific cell populations directly infected by SARS-CoV-2 infection in COVID-19 patients.

**Methodology:** a gene expression analysis on scRNAseq data was provided to explore differential expressed genes (DEG) in specific epithelial cell populations in COVID-19 patient group (mild and severe), comparing with isolated epithelial cells from lungs of healthy subjects. An exploratory gene expression data was carried out on scRNAseq analysis of bronchoalveolar lavages from nine COVID-19 patients, three moderate cases, one severe case and five critical cases (GSE145826) (10.1038/s41591-020-0901-9). To these data, to obtain high confidence of difference expressions in three different groups, single cell RNA-seq data of isolated epithelial cells (DAPI-,CD45-, CD31-,CD326+) from control lung explant tissue of 9 health subjects was chosen as health control specific for epithelial cell types ( 10.1164/rccm.202008-3198OC). All filtered samples, as reported in own papers, were merged in only one filtered gene-barcode matrix and analysed with Seurat v.3 (10.1016/j.cell.2019.05.031). Moreover, the positive DEGs per celltypes  were repoted as overlay in COVID-19 Disaese Map repository and Gene Set Enrichment Analysis was carried out on DEGs per celltypes obtained for specific epithelial celltypes 

